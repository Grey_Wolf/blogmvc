﻿using System.Linq;
using BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace BLLTest
{
    [TestClass]
    public class BllTest
    {
        readonly UnitOfLogic _unitOfLogic = new UnitOfLogic();
        [TestMethod]
        public void TagSplit()
        {
            string entry = " #cats #dogs #people ";
            string[] expected = entry.Trim().Split(' ');
            string[] actual = _unitOfLogic.
                TagService.SplitToTags(entry.ToUpper()).
                Select(t => t.Text).ToArray();
            for (int i = 0; i < 3; i++)
                Assert.AreEqual(expected[i], actual[i]);
        }
    }
}
