﻿namespace WebSite.Models
{
    public class VariantModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int CountOfUse { get; set;}

        public int QuestionNumber { get; set; }

        public int PercentOfUsage { get; set; }
    }
}