﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebSite.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }

        public string Author { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",ApplyFormatInEditMode = true)]
        public DateTime PublicationDate { get; set; }

        [Required(ErrorMessage = "Article can't be untitled")]
        [MinLength(10,ErrorMessage = "Title must be from 10 to 40 characters.")]
        [MaxLength(40,ErrorMessage = "Title must be from 10 to 40 characters.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Article can't be empty")]
        [MinLength(200, ErrorMessage = "Text of article must be at least 200 characters.")]
        public string Text { get; set; }

        public virtual IEnumerable<TagModel> Tags { get; set; }

        public virtual IEnumerable<CommentModel> Comments { get; set; }

        public string TagString { get; set; }
    }
}