﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Web;

namespace WebSite.Models
{
    public class CommentModel
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public int ArticleId { get; set; }

        public int LevelOfNesting { get; set; }

        public string Author { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        public string Text { get; set; }

        public IEnumerable<CommentModel> ChildComments { get; set; }
    }
}