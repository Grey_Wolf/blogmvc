﻿using System.Collections.Generic;

namespace WebSite.Models
{
    public class QuizPassModel 
    {
        public int Id { get; set; }

        public string QuizTitle { get; set; }

        public IEnumerable<AnswerModel> Answers { get; set; }

        public string Author { get; set; }

    }
}