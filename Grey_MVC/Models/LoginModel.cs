﻿using System.ComponentModel.DataAnnotations;

namespace WebSite.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Type the login into the field")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Type the password into the field")]
        public string Password { get; set; }

    }
}