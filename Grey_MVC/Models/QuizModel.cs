﻿using System.Collections.Generic;

namespace WebSite.Models
{
    public class QuizModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public IEnumerable<QuestionModel> Questions { get; set; }
    }
}