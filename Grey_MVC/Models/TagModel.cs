﻿using System.Collections.Generic;

namespace WebSite.Models
{
    public class TagModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public virtual IEnumerable<ArticleModel> Articles { get; set; }
    }
}