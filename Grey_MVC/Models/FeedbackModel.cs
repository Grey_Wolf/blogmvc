﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebSite.Models
{
    public class FeedbackModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Feedback must have an author")]
        public string Author { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",ApplyFormatInEditMode = true)]
        public DateTime PublicationDate { get; set; }

        [Required(ErrorMessage = "Feedback can't be empty")]
        public string Text { get; set; }
    }
}