﻿using System.Collections.Generic;
using Contracts.Entities;

namespace WebSite.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int NumberOfQuestion { get; set; }

        public QuestionType Type { get; set; }

        public IEnumerable<VariantModel> Variants { get; set; }
    }
}