﻿using System.Web.Mvc;
using BLL.AuthProvider;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthProvider _authProvider;

        public AccountController()
        {
            _authProvider = new FormAuthProvider();
        }
        // GET: Account
        public ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (_authProvider.Authenticate(model.Login, model.Password))
                {
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Article");
                }
                ModelState.AddModelError("","Wrong login or/and password");
                return View();
            }
            return View(model);
        }
    }
}