﻿using System;
using System.Linq;
using System.Web.Mvc;
using BLL;
using Contracts.Entities;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class QuizController : Controller
    {
        private readonly UnitOfLogic _unitOfLogic;
        public QuizController()
        {
            _unitOfLogic = new UnitOfLogic();
        }

        [Authorize]
        public ActionResult Index()
        {
            var allQuiz = _unitOfLogic.QuizService.GetAll();
            var quizModel = allQuiz.Select(quizEntity => new QuizModel
            {
                Id = quizEntity.Id,
                Title = quizEntity.Title
            }).ToList();
            return View(quizModel);
        }
        [Authorize]
        public ActionResult Detail(int id)
        {
            var quiz = _unitOfLogic.QuizService.GetById(id);
            var quizModel = new QuizModel
            {
                Id = quiz.Id,
                Title = quiz.Title,
                Questions = quiz.Questions.Select(q => new QuestionModel
                {
                    Id = q.Id,
                    NumberOfQuestion = q.NumberOfQuestion,
                    Text = q.Text,
                    Type = q.Type,
                    Variants = q.Variants.Select(v => new VariantModel
                    {
                        Id = v.Id,
                        Text = v.Text,
                        QuestionNumber = v.Question.NumberOfQuestion
                    })
                })
            };
            return View(quizModel);
        }

        [HttpPost]
        public ActionResult Detail(FormCollection form)
        {
            GetStatisticData(form);
            SavePass(form);
            return RedirectToAction("Statistics", new { id = int.Parse(form["quizId"]) });
        }

        public ActionResult Statistics(int id)
        {
            var quiz = _unitOfLogic.QuizService.GetById(id);
            var countOfUsersPass = _unitOfLogic.
                QuizPassService.GetAll().Count(q => q.QuizId == id);
            var model = new QuizModel
            {
                Id = quiz.Id,
                Title = quiz.Title,
                Questions = quiz.Questions.Select(q => new QuestionModel
                {
                    Id = q.Id,
                    NumberOfQuestion = q.NumberOfQuestion,
                    Text = q.Text,
                    Variants = q.Variants.Select(v => new VariantModel
                    {
                        Id = v.Id,
                        Text = v.Text,
                        CountOfUse = v.CountOfUse,
                        PercentOfUsage = (int)(Math.Round((v.CountOfUse/(double)countOfUsersPass),2)*100)
                    })
                })
            };
            
            ViewBag.CountOfUsers = countOfUsersPass;
            return View(model);
        }

        private void GetStatisticData(FormCollection form)
        {
            var quiz = _unitOfLogic.QuizService.GetById(int.Parse(form["quizId"]));
            foreach (var question in quiz.Questions)
            {
                var answerText = form[question.NumberOfQuestion.ToString()];
                if (question.Type != QuestionType.Text)
                {
                    if (question.Type == QuestionType.SelectMultiple)
                    {
                        var answers = answerText.Split(',').ToList();
                        foreach (var singleAnswer in answers)
                        {
                            var variant = _unitOfLogic.VariantsService.GetAll().
                                First(v => v.QuestionId == question.Id && v.Text.Equals(singleAnswer));
                            variant.CountOfUse++;
                            _unitOfLogic.VariantsService.Update(variant);
                        }
                    }
                    else
                    {
                        var variant = _unitOfLogic.VariantsService.GetAll().
                            First(v => v.QuestionId == question.Id && v.Text.Equals(answerText));
                        variant.CountOfUse++;
                        _unitOfLogic.VariantsService.Update(variant);
                    }
                }
            }
            _unitOfLogic.Save();
        }
        private void SavePass(FormCollection form)
        {
            int quizId = int.Parse(form["quizId"]);
            Quiz quiz = _unitOfLogic.QuizService.GetById(quizId);
            QuizPass quizPass = new QuizPass
            {
                TitleOfQuiz = _unitOfLogic.QuizService.GetById(quizId).Title,
                AuthorId = _unitOfLogic.UserService.GetCurrentUser().Id,
                QuizId = quizId
            };
            _unitOfLogic.QuizPassService.Insert(quizPass);

            foreach (var question in quiz.Questions)
            {
                _unitOfLogic.AnswerService.Insert(new Answer
                {
                    QuizPassId = quizPass.Id,
                    Question = question.Text,
                    TextOfAnswer = form[question.NumberOfQuestion.ToString()]
                });
            }
            _unitOfLogic.Save();
        }
    }
}