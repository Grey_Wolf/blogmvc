﻿using System.Linq;
using System.Web.Mvc;
using BLL;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class QuizPassController : Controller
    {
        private readonly UnitOfLogic _unitOfLogic;
        public QuizPassController()
        {
            _unitOfLogic = new UnitOfLogic();
        }

        public ActionResult Index()
        {
            var quizPasses = _unitOfLogic.QuizPassService.GetAll().ToList();
            var quizPassModels = quizPasses.Select(quizPassEntity => new QuizPassModel
            {
                Id = quizPassEntity.Id,
                QuizTitle = quizPassEntity.TitleOfQuiz,
                Author = quizPassEntity.Author.Name,
                Answers = quizPassEntity.Answers.Select(a => new AnswerModel
                {
                    Id = a.Id,
                    Question = a.Question,
                    Answer = a.TextOfAnswer
                })
            }).ToList();
            return View(quizPassModels);
        }

        public ActionResult Detail(int id)
        {
            var quizPass = _unitOfLogic.QuizPassService.GetById(id);
            var quizPassModel = new QuizPassModel
            {
                Id = quizPass.Id,
                Author = quizPass.Author.Name,
                QuizTitle = quizPass.TitleOfQuiz,
                Answers = quizPass.Answers.Select(a=>new AnswerModel
                {
                    Id = a.Id,
                    Question = a.Question,
                    Answer = a.TextOfAnswer
                })
            };
            return View(quizPassModel);
        }
    }
}