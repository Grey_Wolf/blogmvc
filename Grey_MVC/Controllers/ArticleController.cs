﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BLL;
using Contracts.Entities;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class ArticleController : Controller
    {
        private const int SidebarQuizId = 1,
            DefaultCountOfSimilarTags = 20,
            DefaultTagId = -1,
            DefaultCountOfSimilarArticles = 5;
        private readonly UnitOfLogic _unitOfLogic;

        public ArticleController()
        {
            _unitOfLogic = new UnitOfLogic();
        }
        // GET: Article
        public ActionResult Index(int tagId = DefaultTagId)
        {
            var articleEntities = _unitOfLogic.ArticleService.GetAll();
            
            var articleModels = articleEntities.Select(articleEntity => new ArticleModel
            {
                Id = articleEntity.Id,
                Author = articleEntity.Author.Name,
                Tags = articleEntity.Tags.Select(t=>new TagModel
                {
                    Id = t.Id,
                    Text = t.Text
                }),
                Text = articleEntity.Text,
                Title = articleEntity.Title,
                PublicationDate = articleEntity.PublicationDate
            })
                .OrderByDescending(a=>a.PublicationDate).ToList();

            if (tagId != DefaultTagId)
            {
                articleModels = articleModels.Where(a => a.Tags.
                    Select(t=>t.Id).Contains(tagId)).ToList();
            }

            ViewData["PopularTag"] = PopularTags(DefaultCountOfSimilarTags);
            var quiz = _unitOfLogic.QuizService.GetById(SidebarQuizId);
            ViewData["SidebarQuiz"] = new QuizModel
            {
                Id = quiz.Id,
                Title = quiz.Title,
                Questions = quiz.Questions.Select(q => new QuestionModel
                {
                    Id = q.Id,
                    NumberOfQuestion = q.NumberOfQuestion,
                    Text = q.Text,
                    Type = q.Type,
                    Variants = q.Variants.Select(v => new VariantModel
                    {
                        Id = v.Id,
                        Text = v.Text,
                        QuestionNumber = v.Question.NumberOfQuestion
                    })
                })
            };
            return View(articleModels);
        }
        
        public ActionResult Detail(int id)
        {
            var articleEntity = _unitOfLogic.ArticleService.GetById(id);
            var allComments = _unitOfLogic.CommentService.GetAll().Where(c => c.ArticleId == articleEntity.Id).ToList();
            var commentsModel = allComments.Select(c => new CommentModel()
            {
                Id = c.Id,
                ParentId = c.ParentId,
                LevelOfNesting = c.LevelOfNesting,
                Author = c.Author.Name,
                Date = c.Date,
                Text = c.Text
            }).ToList();
            foreach (var comment in commentsModel)
            {
                comment.ChildComments = commentsModel.Where(c => c.ParentId == comment.Id).ToList();
            }
            var articleModel = new ArticleModel
            {
                Id = articleEntity.Id,
                Author = articleEntity.Author.Name,
                PublicationDate = articleEntity.PublicationDate,
                Tags = articleEntity.Tags.Select(t => new TagModel
                {
                    Id = t.Id,
                    Text = t.Text
                }),
                Title = articleEntity.Title,
                Text = articleEntity.Text,
                Comments = commentsModel.Where(c=>c.ParentId==null).ToList()
            };
            ViewData["Similar"] = SimilarArticles(articleEntity, DefaultCountOfSimilarArticles);
            return View(articleModel);
        }

        [Authorize]
        public ActionResult Create()
        {
            ViewData["PopularTag"] = PopularTags(20);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(ArticleModel newArticle)
        {

            if (!ModelState.IsValid)
            {
                return View();
            }
            _unitOfLogic.ArticleService.Insert(new Article
            {
                Author = _unitOfLogic.UserService.GetCurrentUser(),
                PublicationDate = DateTime.UtcNow,
                Tags = newArticle.TagString != null ? _unitOfLogic.TagService.SplitToTags(newArticle.TagString) : null,
                Title = newArticle.Title,
                Text = newArticle.Text
            });
            _unitOfLogic.Save();
            return RedirectToAction("Index");
        }

        public IEnumerable<TagModel> PopularTags(int count)
        {
            return _unitOfLogic.TagService.GetPopular(count).
                Select(tag => new TagModel
                {
                    Id = tag.Id,
                    Text = tag.Text
                }).ToList();
        }

        public IEnumerable<ArticleModel> SimilarArticles(Article current,int count)
        {
            var similarArticles = _unitOfLogic.ArticleService.GetSimilar(current, count);
            return similarArticles?.Select(a => new ArticleModel()
            {
                Id = a.Id,
                Author = a.Author.Name,
                Title = a.Title
            });
        }

        [HttpPost]
        public ActionResult AddComment(CommentModel newComment)
        {
            _unitOfLogic.CommentService.Insert(new Comment()
            {
                ArticleId = newComment.ArticleId,
                Author = _unitOfLogic.UserService.GetCurrentUser(),
                Date = DateTime.UtcNow,
                LevelOfNesting = (newComment.ParentId != null)
                    ? _unitOfLogic.
                          CommentService.GetById(newComment.ParentId).LevelOfNesting + 1
                    : 0,
                ParentId = newComment.ParentId,
                Text = newComment.Text
            });
            return RedirectToAction("Detail", new {id = newComment.ArticleId});
        }
    }
}