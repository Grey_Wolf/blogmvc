﻿using System;
using System.Linq;
using System.Web.Mvc;
using BLL;
using Contracts.Entities;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class FeedbackController : Controller
    {
        private readonly UnitOfLogic _unitOfLogic;
        public FeedbackController()
        {
            _unitOfLogic = new UnitOfLogic();
        }


        public ActionResult Index()
        {
            var feedbackEntities = _unitOfLogic.FeedbackService.GetAll();
            var feedbackModels = feedbackEntities.
                Select(feedbackEntity => new FeedbackModel
                {
                Id = feedbackEntity.Id,
                Author = feedbackEntity.Author,
                PublicationDate = feedbackEntity.PublicationDate,
                Text = feedbackEntity.Text
            }).OrderByDescending(f=>f.PublicationDate).ToList();
            return View(feedbackModels);
        }
        [HttpPost]
        public ActionResult Index(FeedbackModel newFeedback)
        {
            _unitOfLogic.FeedbackService.Insert(new Feedback
            {
                Author = newFeedback.Author,
                PublicationDate = DateTime.UtcNow,
                Text = newFeedback.Text
            });
            _unitOfLogic.Save();
            return RedirectToAction("Index");
        }
    }
}