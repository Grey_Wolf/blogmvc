﻿function setParentForComment(newParentId) {
    var newCommentParent = document.getElementById("new-comment-parent");
    newCommentParent.setAttribute("value", newParentId);

    var btn = document.createElement('input');
    btn.setAttribute("type", "button");
    btn.setAttribute("onclick", "removeParentFromComment()");
    btn.setAttribute("value", "Disable reply mode");
    btn.setAttribute("class", "btn btn-default");
    btn.setAttribute("id", "cancel-btn");
    newCommentParent.parentElement.appendChild(btn);
}

function removeParentFromComment() {
    var newCommentParent = document.getElementById("new-comment-parent");
    newCommentParent.setAttribute("value", "");

    var cancelBtn = document.getElementById("cancel-btn");
    cancelBtn.parentElement.removeChild(cancelBtn);
}