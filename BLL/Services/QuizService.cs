﻿using System.Collections.Generic;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class QuizService
    {
        private readonly UnitOfWork _unitOfWork;

        public QuizService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Quiz> GetAll()
        {
            return _unitOfWork.QuizzesRepository.GetAll();
        }

        public Quiz GetById(object id)
        {
            return _unitOfWork.QuizzesRepository.GetById(id);
        }

        public void Insert(Quiz newQuiz)
        {
            _unitOfWork.QuizzesRepository.Insert(newQuiz);
        }

        public void Update(Quiz changedQuiz)
        {
            _unitOfWork.QuizzesRepository.Update(changedQuiz);
        }

        public void Delete(object id)
        {
            _unitOfWork.QuizzesRepository.Delete(id);
        }
    }
}
