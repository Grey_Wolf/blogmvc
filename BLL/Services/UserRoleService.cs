﻿using System.Collections.Generic;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class UserRoleService
    {
        private readonly UnitOfWork _unitOfWork;

        public UserRoleService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<UserRole> GetAll()
        {
            return _unitOfWork.RolesRepository.GetAll();
        }

        public UserRole GetById(object id)
        {
            return _unitOfWork.RolesRepository.GetById(id);
        }

        public void Insert(UserRole newRole)
        {
            _unitOfWork.RolesRepository.Insert(newRole);
        }

        public void Update(UserRole changedRole)
        {
            _unitOfWork.RolesRepository.Update(changedRole);
        }

        public void Delete(object id)
        {
            _unitOfWork.RolesRepository.Delete(id);
        }
    }
}
