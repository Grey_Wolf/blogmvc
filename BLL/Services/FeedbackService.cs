﻿using System.Collections.Generic;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class FeedbackService
    {
        private readonly UnitOfWork _unitOfWork;

        public FeedbackService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Feedback> GetAll()
        {
            return _unitOfWork.FeedbackRepository.GetAll();
        }

        public Feedback GetById(object id)
        {
            return _unitOfWork.FeedbackRepository.GetById(id);
        }

        public void Insert(Feedback newFeedback)
        {
            _unitOfWork.FeedbackRepository.Insert(newFeedback);
        }

        public void Update(Feedback updatedFeedback)
        {
            _unitOfWork.FeedbackRepository.Update(updatedFeedback);
        }

        public void Delete(object id)
        {
            _unitOfWork.FeedbackRepository.Delete(id);
        }
    }
}
