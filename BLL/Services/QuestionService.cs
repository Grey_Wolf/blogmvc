﻿using System.Collections.Generic;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class QuestionService
    {
        private readonly UnitOfWork _unitOfWork;

        public QuestionService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Question> GetAll()
        {
            return _unitOfWork.QuestionsRepository.GetAll();
        }

        public Question GetById(object id)
        {
            return _unitOfWork.QuestionsRepository.GetById(id);
        }

        public void Insert(Question newQuestion)
        {
            _unitOfWork.QuestionsRepository.Insert(newQuestion);
        }

        public void Update(Question changedQuestion)
        {
            _unitOfWork.QuestionsRepository.Update(changedQuestion);
        }

        public void Delete(object id)
        {
            _unitOfWork.QuestionsRepository.Delete(id);
        }
    }
}
