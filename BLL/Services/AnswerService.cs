﻿using System.Collections.Generic;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class AnswerService
    {
        private readonly UnitOfWork _unitOfWork;

        public AnswerService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Answer> GetAll()
        {
            return _unitOfWork.AnswersRepository.GetAll();
        }

        public Answer GetById(object id)
        {
            return _unitOfWork.AnswersRepository.GetById(id);
        }

        public void Insert(Answer newAnswer)
        {
            _unitOfWork.AnswersRepository.Insert(newAnswer);
        }

        public void Update(Answer changedAnswer)
        {
            _unitOfWork.AnswersRepository.Update(changedAnswer);
        }

        public void Delete(object id)
        {
            _unitOfWork.AnswersRepository.Delete(id);
        }
    }
}
