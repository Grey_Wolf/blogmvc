﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class CommentService
    {
        private readonly UnitOfWork _unitOfWork;

        public CommentService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Comment> GetAll()
        {
            return _unitOfWork.CommentsRepository.GetAll();
        }

        public Comment GetById(object id)
        {
            return _unitOfWork.CommentsRepository.GetById(id);
        }

        public void Insert(Comment newComment)
        {
            _unitOfWork.CommentsRepository.Insert(newComment);
        }

        public void Update(Comment changedComment)
        {
            _unitOfWork.CommentsRepository.Update(changedComment);
        }

        public void Delete(object id)
        {
            _unitOfWork.CommentsRepository.Delete(id);
        }
    }
}
