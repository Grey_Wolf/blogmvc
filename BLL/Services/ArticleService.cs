﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Contracts.Entities;

namespace BLL.Services
{
    public class ArticleService
    {
        private readonly UnitOfWork _unitOfWork;

        public ArticleService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Article> GetAll()
        {
            return _unitOfWork.ArticleRepository.GetAll();
        }

        public Article GetById(object id)
        {
            return _unitOfWork.ArticleRepository.GetById(id);
        }

        public void Insert(Article newArticle)
        {
            _unitOfWork.ArticleRepository.Insert(newArticle);
        }

        public void Update(Article changedArticle)
        {
            _unitOfWork.ArticleRepository.Update(changedArticle);
        }

        public void Delete(object id)
        {
            _unitOfWork.ArticleRepository.Delete(id);
        }

        public IEnumerable<Article> GetSimilar(Article current, int count)
        {
            var articles = GetAll().
                OrderByDescending(a => a.Tags.Intersect(current.Tags).Count()).
                Where(a => a.Tags.Intersect(current.Tags).Count() != 0 && a.Id != current.Id).ToList();
            if (articles.Count != 0)
            {
                if (articles.Count <= count)
                {
                    return articles;
                }
                return articles.Take(count).ToList();
            }
            return null;
        }
    }
}
