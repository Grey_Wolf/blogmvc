﻿using System.Collections.Generic;
using System.Linq;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class TagService
    {
        private readonly UnitOfWork _unitOfWork;

        public TagService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Tag> GetAll()
        {
            return _unitOfWork.TagsRepository.GetAll();
        }

        public Tag GetById(object id)
        {
            return _unitOfWork.TagsRepository.GetById(id);
        }

        public void Insert(Tag newTag)
        {
            _unitOfWork.TagsRepository.Insert(newTag);
        }

        public void Update(Tag changedTag)
        {
            _unitOfWork.TagsRepository.Update(changedTag);
        }

        public void Delete(object id)
        {
            _unitOfWork.TagsRepository.Delete(id);
        }

        public ICollection<Tag> SplitToTags(string stringWithTags)
        {
            ICollection<Tag> result = new List<Tag>();
            List<string> textTags = stringWithTags.ToLower().Trim().Split(' ').ToList();
            List<Tag> allTags = GetAll().ToList();
            foreach (var textTag in textTags)
            {
                var Tag = allTags.FirstOrDefault(t => t.Text.Equals(textTag));
                if (Tag == null)
                {
                    Tag = new Tag()
                    {
                        Text = textTag
                    };
                    Insert(Tag);
                }
                result.Add(Tag);
            }
            return result;
        }

        public ICollection<Tag> GetPopular(int count)
        {
            List<Tag> allTags = GetAll().OrderByDescending(tag => tag.Articles.Count).ToList();
            if (count <= allTags.Count)
            {
                return allTags;
            }
            return allTags.Take(count).ToList();
        }

    }
}
