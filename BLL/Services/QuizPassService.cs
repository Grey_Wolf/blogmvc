﻿using System.Collections.Generic;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class QuizPassService
    {
        private readonly UnitOfWork _unitOfWork;

        public QuizPassService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<QuizPass> GetAll()
        {
            return _unitOfWork.QuizPassesRepository.GetAll();
        }

        public QuizPass GetById(object id)
        {
            return _unitOfWork.QuizPassesRepository.GetById(id);
        }

        public void Insert(QuizPass newQuizPass)
        {
            _unitOfWork.QuizPassesRepository.Insert(newQuizPass);
        }

        public void Update(QuizPass changedQuizPass)
        {
            _unitOfWork.QuizPassesRepository.Update(changedQuizPass);
        }

        public void Delete(object id)
        {
            _unitOfWork.QuizPassesRepository.Delete(id);
        }
    }
}
