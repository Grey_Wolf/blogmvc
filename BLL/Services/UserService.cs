﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class UserService
    {
        private readonly UnitOfWork _unitOfWork;

        public UserService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<User> GetAll()
        {
            return _unitOfWork.UserRepository.GetAll();
        }

        public User GetById(object id)
        {
            return _unitOfWork.UserRepository.GetById(id);
        }

        public void Insert(User newUser)
        {
            _unitOfWork.UserRepository.Insert(newUser);
        }

        public void Update(User changedUser)
        {
            _unitOfWork.UserRepository.Update(changedUser);
        }

        public void Delete(object id)
        {
            _unitOfWork.UserRepository.Delete(id);
        }

        public User GetCurrentUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return
                    _unitOfWork.UserRepository
                        .GetAll()
                        .First(u => u.Login.Equals(HttpContext.Current.User.Identity.Name));
            }
            return _unitOfWork.UserRepository.GetAll().First(u => u.Name.Equals("untitled"));
        }
    }
}
