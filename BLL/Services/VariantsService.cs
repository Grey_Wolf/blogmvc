﻿using System.Collections.Generic;
using Contracts.Entities;
using DAL;

namespace BLL.Services
{
    public class VariantsService
    {
        private readonly UnitOfWork _unitOfWork;

        public VariantsService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Variant> GetAll()
        {
            return _unitOfWork.VariantsRepository.GetAll();
        }

        public Variant GetById(object id)
        {
            return _unitOfWork.VariantsRepository.GetById(id);
        }

        public void Insert(Variant newVariant)
        {
            _unitOfWork.VariantsRepository.Insert(newVariant);
        }

        public void Update(Variant changedVariant)
        {
            _unitOfWork.VariantsRepository.Update(changedVariant);
        }

        public void Delete(object id)
        {
            _unitOfWork.VariantsRepository.Delete(id);
        }
    }
}
