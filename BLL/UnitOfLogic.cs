﻿using BLL.Services;
using DAL;

namespace BLL
{
    public class UnitOfLogic
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        private UserRoleService _userRoleService;
        private UserService _userService;
        private ArticleService _articleService;
        private FeedbackService _feedbackService;
        private QuizService _quizService;
        private QuizPassService _quizPassService;
        private TagService _tagService;
        private QuestionService _questionService;
        private VariantsService _variantsService;
        private AnswerService _answerService;
        private CommentService _commentService;

        public UserRoleService UserRoleService
        {
            get
            {
                if (_userRoleService == null)
                {
                    _userRoleService = new UserRoleService(_unitOfWork);
                }
                return _userRoleService;
            }
        }
        public UserService UserService
        {
            get
            {
                if (_userService == null)
                {
                    _userService = new UserService(_unitOfWork);
                }
                return _userService;
            }
        }
        public ArticleService ArticleService
        {
            get
            {
                if (_articleService == null)
                {
                    _articleService = new ArticleService(_unitOfWork);
                }
                return _articleService;
            }
        }
        public FeedbackService FeedbackService
        {
            get
            {
                if (_feedbackService == null)
                {
                    _feedbackService = new FeedbackService(_unitOfWork);
                }
                return _feedbackService;
            }
        }
        public QuizService QuizService
        {
            get
            {
                if (_quizService == null)
                {
                    _quizService = new QuizService(_unitOfWork);
                }
                return _quizService;
            }
        }
        public QuizPassService QuizPassService
        {
            get
            {
                if (_quizPassService == null)
                {
                    _quizPassService = new QuizPassService(_unitOfWork);
                }
                return _quizPassService;
            }
        }
        public TagService TagService
        {
            get
            {
                if (_tagService == null)
                {
                    _tagService = new TagService(_unitOfWork);
                }
                return _tagService;
            }
        }
        public QuestionService QuestionService
        {
            get
            {
                if (_questionService == null)
                {
                    _questionService = new QuestionService(_unitOfWork);
                }
                return _questionService;
            }
        }
        public VariantsService VariantsService
        {
            get
            {
                if (_variantsService == null)
                {
                    _variantsService = new VariantsService(_unitOfWork);
                }
                return _variantsService;
            }
        }
        public AnswerService AnswerService
        {
            get
            {
                if (_answerService == null)
                {
                    _answerService = new AnswerService(_unitOfWork);
                }
                return _answerService;
            }
        }
        public CommentService CommentService
        {
            get
            {
                if (_commentService == null)
                {
                    _commentService = new CommentService(_unitOfWork);
                }
                return _commentService;
            }
        }

        public void Save()
        {
            _unitOfWork.Save();
        }

    }
}
