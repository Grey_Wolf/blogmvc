﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.AuthProvider
{
    public interface IAuthProvider
    {
        bool Authenticate(string login, string password);

        void DeAuthenticate();
    }
}
