﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using DAL;

namespace BLL.AuthProvider
{
    public class FormAuthProvider:IAuthProvider
    {
        public bool Authenticate(string login, string password)
        {
            AccountProvider accountProvider = new AccountProvider();
            if (accountProvider.Login(login, password))
            {
                FormsAuthentication.SetAuthCookie(userName:login,createPersistentCookie:true);
                return true;
            }
            return false;
        }

        public void DeAuthenticate()
        {
            FormsAuthentication.SignOut();
        }
    }
}
