﻿namespace Contracts.Entities
{
    public enum QuestionType
    {
        Text,
        RadioButton,
        SelectOne,
        SelectMultiple
    }
}
