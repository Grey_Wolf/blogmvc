﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }
        
        [ForeignKey("Role")]
        public int RoleId { get; set; }
        public virtual UserRole Role { get; set; }

        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<Quiz> Quizzes { get; set; }
    }
}
