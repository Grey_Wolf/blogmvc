﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.Entities
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Author")]
        public int AuthorId { get; set; }
        public virtual User Author { get; set; }
        
        public int LevelOfNesting { get; set; }

        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        public Comment Parent { get; set; }

        public int ArticleId { get; set; }
        public virtual Article Article { get; set; }

        public DateTime Date { get; set; }

        public string Text { get; set; }

        public virtual ICollection<Comment> ChildComments { get; set; }

    }
}
