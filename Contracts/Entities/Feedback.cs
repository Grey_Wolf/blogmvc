﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.Entities
{
    public class Feedback
    {
        [Key]
        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime PublicationDate { get; set; }

        public string Author { get; set; }
    }
}
