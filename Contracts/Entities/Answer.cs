﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.Entities
{
    public class Answer
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("QuizPass")]
        public int QuizPassId { get; set; }
        public virtual QuizPass QuizPass { get; set; }

        public string Question { get; set; }
        public string TextOfAnswer { get; set; }
    }
}
