﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.Entities
{
    public class QuizPass
    {
        [Key]
        public int Id { get; set; }

        public string TitleOfQuiz { get; set; }
        public int QuizId { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }

        [ForeignKey("Author")]
        public int AuthorId { get; set; }
        public virtual User Author { get; set; }
    }
}
