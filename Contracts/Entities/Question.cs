﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contracts.Entities
{
    public class Question
    {
        [Key]
        public int Id { get; set; }

        public int NumberOfQuestion { get; set; }

        public QuestionType Type { get; set; }

        public string Text { get; set; }

        [ForeignKey("Quiz")]
        public int QuizId { get; set; }
        public virtual Quiz Quiz { get; set; }

        public virtual ICollection<Variant> Variants { get; set; }
    }


}
