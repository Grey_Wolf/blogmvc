﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts.Entities
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }

        public string Text { get; set; }

        public virtual ICollection<Article> Articles { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
