﻿using System.ComponentModel.DataAnnotations;

namespace Contracts.Entities
{
    public class UserRole
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
