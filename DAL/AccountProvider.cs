﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Database;
namespace DAL
{
    public class AccountProvider
    {
        private AppDbContext db = new AppDbContext();
        public bool Login(string login, string password)
        {
            var account = db.Users.FirstOrDefault(p => 
                    String.Compare(login, p.Login) == 0 &&
                    p.Password.Equals(password));
            if (account != null)
            {
                return true;
            }
            return false;
        }
    }
}
