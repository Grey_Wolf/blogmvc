﻿using System;
using System.Data.Entity;
using Contracts.Entities;

namespace DAL.Database
{
    internal class AppDbInitializer:DropCreateDatabaseIfModelChanges<AppDbContext>
    {
        protected override void Seed(AppDbContext db)
        {
            db.UserRoles.Add(new UserRole()
            {
                Id = 1,
                Title = "Admin"
            });
            db.UserRoles.Add(new UserRole()
            {
                Id = 2,
                Title = "User"
            });

            db.Users.Add(new User()
            {
                Id = 0,
                Name = "untitled",
                Login = "login",
                Password = "password",
                RoleId = 2
            });
            db.Users.Add(new User()
            {
                Id = 1,
                Name = "Nick",
                Login = "Nick",
                Password = "StrongPass",
                RoleId = 1
            });
            db.Users.Add(new User()
            {
                Id = 2,
                Name = "John",
                Login = "John",
                Password = "jpass",
                RoleId = 2
            });
            db.Users.Add(new User()
            {
                Id = 3,
                Name = "Mike",
                Login = "Mike",
                Password = "password",
                RoleId = 2
            });

            db.Articles.Add(new Article()
            {
                Id = 1,
                AuthorId = 1,
                PublicationDate = DateTime.UtcNow,
                Title = "Article Example 1",
                Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                       "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                       "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
                       "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in " +
                       "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " +
                       "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa " +
                       "qui officia deserunt mollit anim id est laborum."
            });
            db.Articles.Add(new Article()
            {
                Id = 2,
                AuthorId = 2,
                PublicationDate = DateTime.UtcNow,
                Title = "Article Example 2",
                Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                       "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                       "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
                       "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in " +
                       "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " +
                       "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa " +
                       "qui officia deserunt mollit anim id est laborum."
            });
            db.Articles.Add(new Article()
            {
                Id = 3,
                AuthorId = 3,
                PublicationDate = DateTime.UtcNow,
                Title = "Article Example 3",
                Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                       "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                       "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
                       "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in " +
                       "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " +
                       "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa " +
                       "qui officia deserunt mollit anim id est laborum."
            });
            db.Articles.Add(new Article()
            {
                Id = 4,
                AuthorId = 1,
                PublicationDate = DateTime.UtcNow,
                Title = "Article Example 4",
                Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                       "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                       "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris " +
                       "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in " +
                       "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " +
                       "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa " +
                       "qui officia deserunt mollit anim id est laborum."
            });

            db.Comments.Add(new Comment()
            {
                Id = 1,
                ArticleId = 1,
                AuthorId = 1,
                Date = DateTime.UtcNow,
                ParentId = null,
                Text = "Cofee, tea, light drugs"
            });
            db.Comments.Add(new Comment()
            {
                Id = 2,
                ArticleId = 1,
                AuthorId = 2,
                Date = DateTime.UtcNow,
                ParentId = null,
                Text = "4ch, pikabu, 2ch"
            });
            db.Comments.Add(new Comment()
            {
                Id = 3,
                ArticleId = 1,
                AuthorId = 3,
                Date = DateTime.UtcNow,
                ParentId = 1,
                Text = "lurkmore forever!!!11"
            });
            db.Comments.Add(new Comment()
            {
                Id = 4,
                ArticleId = 2,
                AuthorId = 1,
                Date = DateTime.UtcNow,
                ParentId = null,
                Text = "Cats, drugs"
            });

            db.Feedbacks.Add(new Feedback()
            {
                Id = 1,
                Author = "Some visitor #1",
                PublicationDate = DateTime.UtcNow,
                Text = "Cool blog"
            });
            db.Feedbacks.Add(new Feedback()
            {
                Id = 2,
                Author = "Some visitor #2",
                PublicationDate = DateTime.UtcNow,
                Text = "Articles is good"
            });
            db.Feedbacks.Add(new Feedback()
            {
                Id = 3,
                Author = "Some visitor #3",
                PublicationDate = DateTime.UtcNow,
                Text = "Where I can subscribe??"
            });
            db.Feedbacks.Add(new Feedback()
            {
                Id = 4,
                Author = "Some visitor #4",
                PublicationDate = DateTime.UtcNow,
                Text = "Write more!"
            });

            db.Quizzes.Add(new Quiz()
            {
                Id = 1,
                AuthorId = 1,
                Title = "Visitors"
            });
            db.Quizzes.Add(new Quiz()
            {
                Id = 2,
                AuthorId = 1,
                Title = "Home pets"
            });

            db.Questions.Add(new Question()
            {
                Id = 1,
                NumberOfQuestion = 1,
                QuizId = 1,
                Text = "What is your gender?",
                Type = QuestionType.RadioButton
            }); //Quiz #1
            db.Questions.Add(new Question()
            {
                Id = 2,
                NumberOfQuestion = 2,
                QuizId = 1,
                Text = "Select your age: ",
                Type = QuestionType.SelectOne
            });

            db.Questions.Add(new Question()
            {
                Id = 3,
                NumberOfQuestion = 1,
                QuizId = 2,
                Text = "What is your name?",
                Type = QuestionType.Text
            }); //Quiz #2
            db.Questions.Add(new Question()
            {
                Id = 4,
                NumberOfQuestion = 2,
                QuizId = 2,
                Text = "Do you have a pet(s)?",
                Type = QuestionType.RadioButton
            }); 
            db.Questions.Add(new Question()
            {
                Id = 5,
                NumberOfQuestion = 3,
                QuizId = 2,
                Text = "Where are you live?",
                Type = QuestionType.SelectOne
            }); 
            db.Questions.Add(new Question()
            {
                Id = 6,
                NumberOfQuestion = 4,
                QuizId = 2,
                Text = "What pet(s) do you prefer?",
                Type = QuestionType.SelectMultiple
            });

            db.Variants.Add(new Variant()
            {
                Id = 1,
                CountOfUse = 0,
                QuestionId = 1,
                Text = "Man"
            });
            db.Variants.Add(new Variant()
            {
                Id = 2,
                CountOfUse = 0,
                QuestionId = 1,
                Text = "Woman"
            });

            db.Variants.Add(new Variant()
            {
                Id = 3,
                CountOfUse = 0,
                QuestionId = 2,
                Text = "<16"
            });
            db.Variants.Add(new Variant()
            {
                Id = 4,
                CountOfUse = 0,
                QuestionId = 2,
                Text = "16-21"
            });
            db.Variants.Add(new Variant()
            {
                Id = 5,
                CountOfUse = 0,
                QuestionId = 2,
                Text = "21-27"
            });
            db.Variants.Add(new Variant()
            {
                Id = 6,
                CountOfUse = 0,
                QuestionId = 2,
                Text = ">27"
            });

            db.Variants.Add(new Variant()
            {
                Id = 7,
                CountOfUse = 0,
                QuestionId = 4,
                Text = "Yes"
            });
            db.Variants.Add(new Variant()
            {
                Id = 8,
                CountOfUse = 0,
                QuestionId = 4,
                Text = "No"
            });

            db.Variants.Add(new Variant()
            {
                Id = 9,
                CountOfUse = 0,
                QuestionId = 5,
                Text = "My own home"
            });
            db.Variants.Add(new Variant()
            {
                Id = 10,
                CountOfUse = 0,
                QuestionId = 5,
                Text = "Appartent"
            });
            db.Variants.Add(new Variant()
            {
                Id = 11,
                CountOfUse = 0,
                QuestionId = 5,
                Text = "Student hostel"
            });

            db.Variants.Add(new Variant()
            {
                Id = 13,
                CountOfUse = 0,
                QuestionId =6,
                Text = "Cat"
            });
            db.Variants.Add(new Variant()
            {
                Id = 14,
                CountOfUse = 0,
                QuestionId = 6,
                Text = "Dog"
            });
            db.Variants.Add(new Variant()
            {
                Id = 15,
                CountOfUse = 0,
                QuestionId = 6,
                Text = "Bird"
            });
            db.Variants.Add(new Variant()
            {
                Id = 16,
                CountOfUse = 0,
                QuestionId = 6,
                Text = "Crocodile"
            });

            db.SaveChanges();

            base.Seed(db);
        }
    }
}
