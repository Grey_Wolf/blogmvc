﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Contracts.Entities;

namespace DAL.Database
{
    public class AppDbContext:DbContext
    {
        public AppDbContext() : base("AppDatabase")
        {
            System.Data.Entity.Database.SetInitializer( new AppDbInitializer());
            Database.Initialize(true);
        }

        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<User> Users { get; set; }

        //all about Articles
        public DbSet<Article> Articles { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public DbSet<Feedback> Feedbacks { get; set; }

        //All about Quizzes
        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Variant> Variants { get; set; }

        //All about QuizPasses
        public DbSet<QuizPass> QuizPasses { get; set; }
        public DbSet<Answer> Answers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }
    }
}
