﻿using System;
using DAL.Repository;
using DAL.Database;
using Contracts.Entities;

namespace DAL
{
    public class UnitOfWork
    {
        private readonly AppDbContext _db = new AppDbContext();

        private GenericRepository<UserRole> _rolesRepository; 
        private GenericRepository<User> _userRepository;
        private GenericRepository<Article> _articleRepository; 
        private GenericRepository<Feedback> _feedbackRepository;
        private GenericRepository<Quiz> _quizzesRepository;
        private GenericRepository<QuizPass> _quizPassesRepository;
        private GenericRepository<Question> _questionsRepository;  
        private GenericRepository<Variant> _variantsRepository;  
        private GenericRepository<Answer> _answersRepository;
        private GenericRepository<Tag> _tagsRepository;
        private GenericRepository<Comment> _commentsRepository;

        public GenericRepository<UserRole> RolesRepository
        {
            get
            {
                if (_rolesRepository == null)
                {
                    _rolesRepository = new GenericRepository<UserRole>(_db);
                }
                return _rolesRepository;
            }
        }
        public GenericRepository<User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new GenericRepository<User>(_db);
                }
                return _userRepository;
            }
        }
        public GenericRepository<Article> ArticleRepository
        {
            get
            {
                if (_articleRepository == null)
                {
                    _articleRepository = new GenericRepository<Article>(_db);
                }
                return _articleRepository;
            }
        }
        public GenericRepository<Feedback> FeedbackRepository
        {
            get
            {
                if (_feedbackRepository == null)
                {
                    _feedbackRepository = new GenericRepository<Feedback>(_db);
                }
                return _feedbackRepository;
            }
        }
        public GenericRepository<Quiz> QuizzesRepository
        {
            get
            {
                if (_quizzesRepository == null)
                {
                    _quizzesRepository = new GenericRepository<Quiz>(_db);
                }
                return _quizzesRepository;
            }
        }
        public GenericRepository<Question> QuestionsRepository
        {
            get
            {
                if (_questionsRepository == null)
                {
                    _questionsRepository = new GenericRepository<Question>(_db);
                }
                return _questionsRepository;
            }
        }
        public GenericRepository<Variant> VariantsRepository
        {
            get
            {
                if (_variantsRepository == null)
                {
                    _variantsRepository = new GenericRepository<Variant>(_db);
                }
                return _variantsRepository;
            }
        }
        public GenericRepository<QuizPass> QuizPassesRepository
        {
            get
            {
                if (_quizPassesRepository == null)
                {
                    _quizPassesRepository = new GenericRepository<QuizPass>(_db);
                }
                return _quizPassesRepository;
            }
        }
        public GenericRepository<Answer> AnswersRepository
        {
            get
            {
                if (_answersRepository == null)
                {
                    _answersRepository = new GenericRepository<Answer>(_db);
                }
                return _answersRepository;
            }
        }
        public GenericRepository<Tag> TagsRepository
        {
            get
            {
                if (_tagsRepository == null)
                {
                    _tagsRepository = new GenericRepository<Tag>(_db);
                }
                return _tagsRepository;
            }
        }
        public GenericRepository<Comment> CommentsRepository
        {
            get
            {
                if (_commentsRepository == null)
                {
                    _commentsRepository = new GenericRepository<Comment>(_db);
                }
                return _commentsRepository;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
